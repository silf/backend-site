#!/bin/sh

cmd="$@"

while true; do
    if psql -tAc "SELECT 1";  then
        break
    else
        echo "Waiting for the database connection"
        sleep 1;
    fi
done

if test ${TIGASE_SETUP_DB-true} = true; then
    while true; do
        # NOTE: we don't check schema version --- as we don't support schema updating.
        # we just check if schema is there.
        if psql -tAc "select tiggetdbproperty('schema-version')";  then
            break
        else
            echo "Waiting for schema to be installed"
            sleep 1;
        fi
    done
fi
set -e

echo "$FAV_ICO" | base64 --decode > /app/site/assets/images/favicon.ico

cd site

mkdir -p ${STATIC_ROOT}

if test ${FAKE_INITIAL-false} = true; then
    echo "FAKE_INITIAL set to true"
    ./manage.py migrate --fake-initial
else
    echo "FAKE_INITIAL set to false"
    ./manage.py migrate
fi

./manage.py collectstatic -v0 --noinput
./manage.py compress --force

chown -R silfapp:silfapp ${STATIC_ROOT}

python ../scripts/update_admin.py

cd ..

exec $cmd

