from django.conf.urls import url

from ilf_app.views import contact

urlpatterns = [
    url(r'^contact$', contact, name='contact'),
]
