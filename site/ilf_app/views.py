# -*- coding: utf-8 -*-

from django.contrib import messages
from django.core.mail import send_mail
from django.shortcuts import render, redirect

from ilf_app.forms import *
from ilf_app.model_forms import *
from ilf_app.models import *


def index(request):
    return render(request, "index.html", locals())


def contact(request):
    try:
        send_mail(u'Kontakt WWW',
              request.POST.get('body', ''),
              u'%s <%s>' % (request.POST.get('name', ''), request.POST.get('email', '')),
              ['silf.mailer@gmail.com'], fail_silently=False)
        messages.success(request, u'Dziękujemy za kontakt. Wiadomość została wysłana')
    except:
        messages.error(request, u'Niestety nie udało się wysłać wiadomości')
    return redirect(request.REQUEST.get('next', ''))
