import os


def maybe_set_env(name, value):
    if name not in os.environ:
        os.environ[name] = value

maybe_set_env('SECRET_KEY', 'NajVartutheth1FlophkejwadHerudEinAwAkjiafyalvamGecyuWratWuTotEcheic8GraftyefUsArrUkyuaphuspOSwiEpenHejdagJakespItAsBiergOtpherEn')
maybe_set_env('FACEBOOK_KEY', 'ok')
maybe_set_env('FACEBOOK_SECRET', 's3cre7')

from .base import *

DEBUG = True
DEBUG_PROPAGATE_EXCEPTIONS = True

TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

DEFAULT_FROM_EMAIL = 'robot@silf.pl'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_FILE_PATH = '/tmp/app-messages'  # change this to a proper location

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': get_env_variable('PGDATABASE'),
        'USER': get_env_variable('PGUSER'),
        'PASSWORD': get_env_variable('PGPASSWORD'),
        'HOST': get_env_variable('PGHOST'),
        'PORT': get_env_variable('PGPORT')
    }
}

MIDDLEWARE += (
    'corsheaders.middleware.CorsMiddleware',
)


class EveryIp(list):

    def __contains__(self, ip):
        return True


INTERNAL_IPS = EveryIp()


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'ilf',
    }
}

# TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

LOGGING = {
    'version' : 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        }
    },
    'loggers': {
        'django': {
            'handlers': ['null'],
            'propagate': True,
            'level': 'INFO',
        },
        'south': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False
        }
    }
}

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True

SESSION_COOKIE_DOMAIN = get_env_variable('FRONTEND_DOMAIN')
CSRF_COOKIE_DOMAIN = get_env_variable('FRONTEND_DOMAIN')
CSRF_TRUSTED_ORIGINS = ['localhost', get_env_variable('FRONTEND_DOMAIN'), get_env_variable('FRONTEND_DOMAIN')+':'+get_env_variable('FRONTEND_HTTPS_PORT')]

COMPRESS_ENABLED = False

TIGASE_SETUP_DB = get_env_variable("TIGASE_SETUP_DB", "true").lower() == "true"

ALLOWED_HOSTS = (
    'localhost',
    'backend',
    '*'
)

# uncomment below lines to have sso from olcms. In this case on your developer
# environment both applications have to be running: olcms and silf. And
# you will be able to login to silf by sso from olcms.
SSO_PROVIDER = 'olcms'
SSO_SECRET = 'q12we34r5t6yu78io90plmknjbhvgcfxdzsa'
SSO_URL = 'http://olcms.dev.local:8000/silf/sso/'
