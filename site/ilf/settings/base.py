# Django settings for efizyka project.

from unipath import Path
from django.utils.translation import ugettext_lazy

_ = lambda s : s


PROJECT_PATH = Path(__file__).ancestor(3)

########################__
# import env variables #
########################

import os
from django.core.exceptions import ImproperlyConfigured

msg = 'Set the %s environment variable'

def get_env_variable(var_name, default=None):
    try:
        return os.environ[var_name]
    except KeyError:
        if default is None:
            error_msg = msg % var_name
            raise ImproperlyConfigured(error_msg)
        else:
            return default


MEDIA_ROOT = get_env_variable('MEDIA_ROOT', PROJECT_PATH.child('media'))
STATIC_ROOT = get_env_variable('STATIC_ROOT', PROJECT_PATH.child('static'))

STATICFILES_DIRS = (
    PROJECT_PATH.child('assets'),
)

MEDIA_URL = '/media/'
STATIC_URL = '/static/'

SILF_EXPERIMENT_MAX_RESERVATION_LENGTH = 3600

ADMINS = (
    ('', ''),
)

MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
### Set to None so that Django will use the same timezone as the os. This is
### specific to Unix based-systems. Read above ^
TIME_ZONE = 'Europe/Warsaw'

USE_TZ = True

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'pl'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True


# Additional locations of static files

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'compressor.finders.CompressorFinder',
)

# Make this unique, and don't share it with anybody.


# List of callables that know how to import templates from various sources.


MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.common.CommonMiddleware'
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            PROJECT_PATH.child('templates'),
        ],
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',

                'django.template.context_processors.request',
                'django.template.context_processors.csrf',
                'sekizai.context_processors.sekizai',
                'ilf.utils.sso_provider',
            ],

            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                # 'django.template.loaders.eggs.Loader',
            ]
        },
    },
]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)


ROOT_URLCONF = 'ilf.urls'

WSGI_APPLICATION = 'ilf.wsgi.application'


# Uncomment any apps you want to use. We will not
# install all apps by default, but all of the
# commented apps are one's we "should" be using.

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django_extensions',

    ####
    # 3rd party apps
    'compressor',
    'sekizai',
    'django_registration',
    'rest_framework',
    'rest_framework.authtoken',  # For token auth
    'widget_tweaks',
    'macros',
    'crispy_forms',
    'django_filters',

    'page_block',

    # silf apps
    'silf.core',
    'silf.reservation',
    'silf.tigase',

    'silf.experiment_results',
    'silf.monitoring',

    # local apps
    'ilf_app',

)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        '': {
            'level': 'DEBUG',
            'handlers': ['console'],
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

#####################
#     COMPRESSOR    #
#####################

COMPRESS_PARSER = 'compressor.parser.HtmlParser'

# NOTE: coffeecompressorcompiler.filter.CoffeeScriptCompiler v0.2.0 fails with mysterious error,
# replaced with system installed coffescript compiler
COMPRESS_PRECOMPILERS = (
    ('text/coffeescript', 'coffee --compile --stdio'),
    ('text/less', 'ilf.utils.LessFilter'),
)

COMPRESS_CSS_FILTERS = [
    "compressor.filters.css_default.CssAbsoluteFilter",
    "compressor.filters.cssmin.CSSMinFilter"
]

COMPRESS_JS_FILTERS = [
    "compressor.filters.jsmin.JSMinFilter"
]


SECRET_KEY = get_env_variable('SECRET_KEY')


TIGASE_SETUP_DB = True


SECURE_PROXY_SSL_HEADER = ("HTTP_X_SCHEME", "https")

#################
#   REDIRECTS   #
#################

LOGIN_REDIRECT_URL = '/'

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'DATETIME_FORMAT': '%Y-%m-%dT%H:%M:%S%z',

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
}

ACCOUNT_ACTIVATION_DAYS = 30


##################################################

LANGUAGE = get_env_variable('LANGUAGE', 'en')

# languages
# ------------------------------------------------------------------------------
LANGUAGES = [
    ('en', ugettext_lazy('English')),
    ('pl', ugettext_lazy('Polish')),
]


TIGASE_SERVER_DOMAIN = get_env_variable('TIGASE_SERVER_DOMAIN')
TIGASE_CONNECTION_URL = get_env_variable('TIGASE_CONNECTION_URL')

# redis configuration
REDIS_HOST = 'redis'
REDIS_PORT = 6379
REDIS_PASSWORD = None

SSO_PROVIDER = get_env_variable("SILF_SSO_PROVIDER", "none")
SSO_SECRET = get_env_variable("SILF_SSO_SECRET", "")
SSO_URL = get_env_variable("SILF_SSO_URL", "")

USE_X_FORWARDED_PORT = True
