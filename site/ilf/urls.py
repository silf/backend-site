from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.views.generic import TemplateView, RedirectView
from django.conf import settings
from django.urls import path

from django.views.static import serve

admin.autodiscover()

urlpatterns = [
    ## Admin
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    path(r'admin/', admin.site.urls),

    url(r'^logout/$', LogoutView.as_view(next_page='/'), name='logout'),

    url(r"^silf/", include("silf.urls")),
    url(r'^accounts/', include('django.contrib.auth.urls')),

    url(r'^app/', include('ilf_app.urls')),

    url(r'^help/$', TemplateView.as_view(template_name="help.html"), name='help'),

    url(r'^$', TemplateView.as_view(template_name="index.html")),

    ## Redirects
    url(r'^favicon.ico$', RedirectView.as_view(url='/static/images/favicon.ico')),
    url(r'^humans.txt$', RedirectView.as_view(url='/static/humans.txt')),
    url(r'^robots.txt$', RedirectView.as_view(url='/static/robots.txt')),
]

if settings.DEBUG:
    urlpatterns = [
        url(r'^media/(?P<path>.*)$', serve,
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        url(r'', include('django.contrib.staticfiles.urls')),
    ] + urlpatterns
