from compressor.filters.base import CompilerFilter
from compressor.filters.css_default import CssAbsoluteFilter
from django.conf import settings


class LessFilter(CompilerFilter):
    def __init__(self, content, attrs, **kwargs):
        super(LessFilter, self).__init__(content, command='lessc {infile} {outfile}', **kwargs)

    def input(self, **kwargs):
        content = super(LessFilter, self).input(**kwargs)
        return CssAbsoluteFilter(content).input(**kwargs)


def sso_provider(request):
    """
    Context processors providing information about used sso provider
    :param request:
    :return: Name of sso provider or 'none' string
    """
    if settings.SSO_PROVIDER:
        return {'sso_provider': settings.SSO_PROVIDER}
    return {'sso_provider': 'none'}
