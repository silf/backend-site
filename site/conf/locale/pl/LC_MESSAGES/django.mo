��            )   �      �  %   �     �     �     �     �  %   �     $     5     T     \     b  /   i     �     �     �     �     �  ;   �  5   %     [  .   o  D   �     �  K   �  *   7     b     {     �  	   �  @  �  &   �          #     3     ;  '   H     p     �     �     �     �  /   �     �     �  
               0   9  +   j  $   �  )   �  D   �     *	  Q   2	  ;   �	     �	     �	     �	     
                                                                           
                   	                                           Activation accomplished successfully. Aktywacja nieudana Aktywacja udana English Experiments Fulfill the form to create an account ILF registration Internet Laboratory of Physics Log in! Login Logout Logowanie za pomocą hasła w systemie E-Fizyka Polish Rejestracja zakończona Reservations Send Some nice words about Dubna... To finish registration process you have to click this link: Unfortunately provided activation key is not correct. Update your browser Welcome inside Internet Laboratory of Physics! Wypełnij poniższy formularz by się zalogować w systemie E-Fizyka Wyślij You are registered now. You have to activate your account. Check you email. You can login again using menu in the top. You have been logged out Your browser is [ILF] Registration very old! Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-07-08 14:49+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Aktywacja zakończyła się pomyślnie Aktywacja nieudana Aktywacja udana English Eksperymenty Wypełnij formularz aby stworzyć konto ILF rejestracja Internetowe Laboratorium Fizyki Zaloguj! Zaloguj Wyloguj Logowanie za pomocą hasła w systemie E-Fizyka Polski Rejestracja zakończona Rezerwacje Wyślij Troche informacji o Dubnej... Aby zakończyć proces aktywacji kliknij w link: Wprowadzony kod aktywacji jest nie poprawny Pobierz nowszą wersje przeglądarki Witamy w Internetowym Laboratorium Fizyki Wypełnij poniższy formularz by się zalogować w systemie E-Fizyka Wyślij Zostałęś zarejestrowany. Musisz aktywować swoje konto. Sprawdz swoją poczte. Możesz zalogować sie ponownie używając powyrzszego menu Zostałeś wylogowany Twoja przeglądarka jest [ILF] Rejestracja bardzo stara 