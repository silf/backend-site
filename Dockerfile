FROM python:3.7

RUN apt-get update && apt-get upgrade -yq &&\
    apt-get install -yq libxml2-dev libxslt-dev postgresql-server-dev-all node-less postgresql-client gettext coffeescript && \
    apt-get clean && rm -rf /var/cache/* && rm -rf /var/tmp/* && rm -rf /tmp/*

WORKDIR /app

RUN mkdir -p /app

# create new user so that root is not used on production env
RUN adduser --disabled-password --gecos "" silfapp && \
    chmod -R o+r /app

COPY requirements /app/requirements
RUN pip install --upgrade pip setuptools && \
    pip install -r requirements/base.txt && \
    rm -rf /root/.cache/pip/

COPY . /app

ENV PGHOST=example.com \
    PGPORT=5432 \
    PGDATABASE=efizyka \
    PGUSER=tigase \
    PGPASSWORD=foo \
    STATIC_ROOT=/www/static \
    MEDIA_ROOT=/www/media \
    DJANGO_SETTINGS_MODULE=ilf.settings.prod \
    PYTHONPATH=.\
    ADMIN_PASSWORD=changemeplease \
    ADMIN_USERNAME=admin \
    EXPERIMENT_CODENAME=example


ENTRYPOINT ["./docker-entrypoint.sh"]

CMD uwsgi --ini uwsgi.ini

VOLUME ["/www/static", "/www/media"]

EXPOSE 8000
