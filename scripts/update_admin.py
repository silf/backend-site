import os
import django
django.setup()

from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from silf.tigase.models import TigaseUser
from silf.core.models import Experiment

from datetime import timedelta

USER_NAME = os.environ['ADMIN_USERNAME']
USER_EMAIL = "admin@example.com"
USER_PASSWORD = os.environ['ADMIN_PASSWORD']

EXP_NAME = os.environ['EXPERIMENT_CODENAME']
TIGASE_DOMAIN = os.environ['TIGASE_DOMAIN']

TOKEN_USER = os.environ['SILF_APP_ATHENA_TOKEN_USER_NAME']
TOKEN = os.environ['SILF_APP_ATHENA_API_TOKEN']


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")

CREATE_EXAMPLE_EXPERIMENT = str2bool(os.environ.get('CREATE_EXAMPLE_EXPERIMENT', 'true'))


def create_user():

    user, created = User.objects.get_or_create(username=USER_NAME)

    user.is_superuser = True
    user.is_active = True
    user.is_staff = True
    user.email = USER_EMAIL
    user.set_password(USER_PASSWORD)

    user.save()


def create_athena_token():

    user = User.objects.get(username=TOKEN_USER)
    if Token.objects.filter(user=user).exists():
        Token.objects.filter(user=user).delete()
    Token.objects.create(user=user, key=TOKEN)


def create_experiment(exp_codename):
    exp, created = Experiment.objects.get_or_create(codename=exp_codename)

    exp.codename = exp_codename
    exp.display_name = 'Example ' + exp_codename
    exp.max_reservation_length = timedelta(hours=2)

    exp.save()


def create_tigase_user(exp_codename):
    uid = exp_codename + '-exp@' + TIGASE_DOMAIN
    usr, created = TigaseUser.objects.get_or_create(user_id=uid)

    usr.user_id = uid
    usr.user_pw = 'test'

    usr.save()


if __name__ == '__main__':
    create_user()
    create_athena_token()
    if CREATE_EXAMPLE_EXPERIMENT:
        create_experiment(EXP_NAME)
        create_tigase_user(EXP_NAME)
