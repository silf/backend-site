# Docker environment variables 


## Compulsory variables

* `DJANGO_SETTINGS_MODULE` – Settings module to use. 
* `SILF_APP_ATHENA_API_TOKEN` – Value for token for athena user
* `SILF_APP_ATHENA_TOKEN_USER_NAME` – Name of the user that will be identified by `SILF_APP_ATHENA_API_TOKEN`.  

* `PGDATABASE` – database to use
* `PGUSER` – database name
* `PGUSER` – database user
* `PGPASSWORD` – database password
* `PGPORT` – defaults to `5432`
* `PGHOST` – database host

 
## Variables used by `settings.py`

Settings py might require other variables.  

* `SECRET_KEY` – secret key for django website
* `EMAIL_USE_SSL` – email server requires SSL
* `EMAIL_HOST` – email server host
* `EMAIL_PORT` – email server port
* `EMAIL_USER` – email server user
* `EMAIL_PASSWORD` – email server password

